const menusSideBar = [
  {
    name: 'Get Started',
    to: '/get-started'
  },
  {
    name: 'Headings',
    to: '/headings'
  },
  {
    name: 'Colors',
    to: '/colors'
  },
  {
    name: 'List',
    to: '/list'
  },
  {
    name: 'Text Alignment',
    to: '/text-alignment'
  },
  {
    name: 'Buttons',
    to: '/buttons'
  },
  {
    name: 'Alert',
    to: '/alert'
  },
  {
    name: 'Card',
    to: '/card'
  },
  {
    name: 'Media Object',
    to: '/media-object'
  },
  {
    name: 'Grid',
    to: '/grid'
  },
  {
    name: 'Table',
    to: '/table'
  },
  {
    name: 'Form',
    to: '/form'
  },
  {
    name: 'Tabs',
    to: '/tabs'
  },
  {
    name: 'Pagination',
    to: '/pagination'
  },
  {
    name: 'Bz Icons',
    to: '/bz-icons'
  },
  {
    name: 'Responsive Media',
    to: '/responsive-media'
  },
  {
    name: 'API',
    to: '/api'
  },
  {
    name: 'API-M',
    to: '/apimount'
  }
]

export default menusSideBar
