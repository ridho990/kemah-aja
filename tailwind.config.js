/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './components/**/*.{js,vue,ts}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
    './app.vue'
  ],
  theme: {
    extend: {
      colors: {
        green: {
          50: '#CEF3E9',
          100: '#A9DACC',
          200: '#89C7B6',
          300: '#6FB6A3',
          400: '#52A08C',
          500: '#3B8D78',
          600: '#2B7B66',
          700: '#1C6C57',
          800: '#0F5A46',
          900: '#064635'
        },
        yellow: {
          50: '#FFF8EC',
          100: '#FCF3E3',
          200: '#F8EAD0',
          300: '#F3E1C3',
          400: '#F1DAB3',
          500: '#F1D4A3',
          600: '#F1CC8E',
          700: '#F1C67F',
          800: '#F1C170',
          900: '#F0BB62'
        },
        danger: {
          100: '#EEABAD',
          200: '#E55459',
          300: '#DD2127'
        },
        success: {
          100: '#A2D7C3',
          200: '#49B78D',
          300: '#028654'
        },
        black: {
          50: '#F2F2F2',
          100: '#CFCFCF',
          200: '#B6B6B6',
          300: '#9E9E9E',
          400: '#868686',
          500: '#6E6E6E',
          600: '#565656',
          700: '#3D3D3D',
          800: '#252525',
          900: '#0D0D0D',
          950: '#000000'
        },
        warning: {
          100: '#FFE599',
          200: '#FFD24D',
          300: '#FFBF00'
        }
      }
    }
  },
  plugins: []
}
